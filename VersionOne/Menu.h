#pragma once
#include "Nivel1.h"
#include "Nivel2.h"
namespace Game {
	using namespace std;
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Diagnostics;	// Debug.WriteLine("mensaje");

	/// <summary>
	/// Clase que da inicio a la selecci�n del Nivel del juego que va a elegir el usuario
	/// </summary>
	public ref class Menu : public System::Windows::Forms::Form
	{
		String^ level;
	public:
		Menu(void)
		{
			InitializeComponent();
			this->CenterToScreen();			// Centrar a la pantalla
		}

	protected:
		~Menu()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^ btnStart;
	private: System::Windows::Forms::Button^ btnSalir;
	private: System::Windows::Forms::ComboBox^ cbLevel;
	private: System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		   /// <summary>
		   /// Erforderliche Methode f�r die Designerunterst�tzung.
		   /// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		   /// </summary>
		   void InitializeComponent(void)
		   {
			   this->btnStart = (gcnew System::Windows::Forms::Button());
			   this->btnSalir = (gcnew System::Windows::Forms::Button());
			   this->cbLevel = (gcnew System::Windows::Forms::ComboBox());
			   this->SuspendLayout();
			   // 
			   // btnStart
			   // 
			   this->btnStart->Location = System::Drawing::Point(133, 119);
			   this->btnStart->Name = L"btnStart";
			   this->btnStart->Size = System::Drawing::Size(121, 21);
			   this->btnStart->TabIndex = 0;
			   this->btnStart->Text = L"Iniciar juego";
			   this->btnStart->UseVisualStyleBackColor = true;
			   this->btnStart->Click += gcnew System::EventHandler(this, &Menu::btnStart_Click);
			   // 
			   // btnSalir
			   // 
			   this->btnSalir->Location = System::Drawing::Point(133, 183);
			   this->btnSalir->Name = L"btnSalir";
			   this->btnSalir->Size = System::Drawing::Size(121, 21);
			   this->btnSalir->TabIndex = 1;
			   this->btnSalir->Text = L"Salir del juego";
			   this->btnSalir->UseVisualStyleBackColor = true;
			   this->btnSalir->Click += gcnew System::EventHandler(this, &Menu::btnSalir_Click);
			   // 
			   // cbLevel
			   // 
			   this->cbLevel->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			   this->cbLevel->FormattingEnabled = true;
			   this->cbLevel->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"Nivel 1", L"Nivel 2" });
			   this->cbLevel->Location = System::Drawing::Point(133, 55);
			   this->cbLevel->Name = L"cbLevel";
			   this->cbLevel->Size = System::Drawing::Size(121, 21);
			   this->cbLevel->TabIndex = 2;
			   this->cbLevel->SelectedIndexChanged += gcnew System::EventHandler(this, &Menu::cbLevel_SelectedIndexChanged);
			   // 
			   // Menu
			   // 
			   this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			   this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			   this->ClientSize = System::Drawing::Size(384, 261);
			   this->Controls->Add(this->cbLevel);
			   this->Controls->Add(this->btnSalir);
			   this->Controls->Add(this->btnStart);
			   this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			   this->MaximizeBox = false;
			   this->Name = L"Menu";
			   this->Text = L"Juego !!!";
			   this->Load += gcnew System::EventHandler(this, &Menu::Menu_Load);
			   this->ResumeLayout(false);

		   }
#pragma endregion

	private: System::Void btnStart_Click(System::Object^ sender, System::EventArgs^ e) {
		Debug::WriteLine("Let's start the game!");
		Debug::WriteLine("Nivel elegido : " + level);
		if (level->Equals("Nivel 1")) {
			this->Hide();
			Nivel1^ nivel1 = gcnew Nivel1();
			nivel1->ShowDialog();
			this->Show();
		}
		if (level->Equals("Nivel 2")) {
			this->Hide();
			Nivel2^ nivel2 = gcnew Nivel2();
			nivel2->ShowDialog();
			this->Show();
		}
	}
	private: System::Void btnSalir_Click(System::Object^ sender, System::EventArgs^ e) {
		Debug::WriteLine("Exit game!");
		this->Close();
	}
	private: System::Void cbLevel_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		level = cbLevel->Text;
	}
	private: System::Void Menu_Load(System::Object^ sender, System::EventArgs^ e) {
		cbLevel->SelectedIndex = 0;
	}
	};
}
