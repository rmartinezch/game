#pragma once
using namespace System::Drawing;

/* Clase que representa al jugador en el juego*/
class Gamer {

	int x, y;		// posici�n (x, y)
	int w, h;		// (ancho, alto)
	int dx, dy;		// avance del paso del Gamer en ambos ejes
	int idx, idy;
	int vidas;
	int puntaje;
	char direccion;
public:
	// La clase se instancia con 4 parametros que son medidas de posici�n y tama�o
	Gamer(int _w, int _h, int _x, int _y) {

		x = _x;
		y = _y;
		w = _w;
		h = _h;
		dx = 30;
		dy = 30;
		idx = 0;
		idy = 0;	// orientaci�n que define la imagen del gamer seleccionado
		vidas = 5;
		puntaje = 0;
		direccion = 'D';
	}

	/* Secci�n del archivo PNG utilizado para el jugador */
	void dibujar(Graphics^ graphic, Bitmap^ bmp) {
		Rectangle seccion = Rectangle(idx * w, idy * h, w, h);
		Rectangle zoom = Rectangle(x, y, w, h);
		graphic->DrawImage(bmp, zoom, seccion, GraphicsUnit::Pixel);
	};

	/* Movimiento del PNG, donde cambia e */
	void mover(Graphics^ graphic, char c) {
		if (c.Equals('D') && ((y + h) < graphic->VisibleClipBounds.Height))
		{
			y += dy;			// aumenta el paso dy (hacia abajo)
			idy = 0;			// direcci�n hacia abajo en el png (bitmap)
			direccion = 'D';
		}
		if (c.Equals('L') && (0 < x))
		{
			x -= dx;
			idy = 1;
			direccion = 'L';
		}
		if (c.Equals('R') && ((x + w) < graphic->VisibleClipBounds.Width))
		{
			x += dx;
			idy = 2;
			direccion = 'R';
		}
		if (c.Equals('U') && (0 < y))
		{
			y -= dy;
			idy = 3;
			direccion = 'U';
		}
		idx++;
		if (2 < idx) { idx = 0; }
		//Debug::WriteLine("Gamer | dx: " + dx + ", x: " + x + "| dy: " + dy + ", y: " + y);
	};

	Rectangle getRectangle() { return Rectangle(x, y, w, h); };

	void setDireccion(char _direccion) { direccion = _direccion; };

	char getDireccion() { return direccion; };

	int getX() { return x; };

	int getY() { return y; };

	int getVidas() { return vidas; };

	void reduceVidas() { vidas--; };

	int getPuntaje() { return puntaje; };

	void addPuntaje(int aumento) { puntaje += aumento; };

protected:
	~Gamer() {}
};