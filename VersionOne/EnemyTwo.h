#pragma once
using namespace System::Drawing;

/* Enemigo que se mueve horizontal y verticalmente */
class EnemyTwo {
	int w, h;		// ancho y alto
	int x, y;		// posicion
	int dx, dy;		// 
	int idx, idy;
	bool visible;
	char direccion;
public:
	EnemyTwo(int ancho, int alto) {
		w = ancho;
		h = alto;
		x = rand() % (784 - 10 - 10 + 1) + 10;	// posici�n inicial aleatoria
		y = rand() % (561 - 10 - 10 + 1) + 10;
		dx = 20;
		dy = 50;
		idx = 0;
		idy = 2;
		visible = true;
		direccion = 0;
	}

	/* Dibujar el enemigo */
	void draw(Graphics^ graphic, Bitmap^ bitmap) {
		Rectangle seccion = Rectangle(w * idx, h * idy, w, h);						// secci�n obtenida proveniente del archivo png (bitmap)
		Rectangle zoom = Rectangle(x, y, w, h);										// posici�n y dimensi�n del enemigo
		graphic->DrawImage(bitmap, zoom, seccion, GraphicsUnit::Pixel);
	}

	/* Movimiento del enemigo de forma autom�tica */
	void move(Graphics^ graphic) {
		// Este enemigo se mueve horizontal y verticalmente
		direccion = rand() % 4;
		if (direccion == 0 && ((y + h) <= graphic->VisibleClipBounds.Height))	{ idy = 0; y += dy; }
		if (direccion == 1 && (0 <= x))											{ idy = 1; x -= dx; }
		if (direccion == 2 && ((x + w) <= graphic->VisibleClipBounds.Width))	{ idy = 2; x += dx; }
		if (direccion == 3 && (0 <= y))											{ idy = 3; y -= dy; }

		idx++;
		if (2 < idx) { idx = 0; }
		//Debug::WriteLine("EnemyOne | dx: " + dx + ", x: " + x + "| dy: " + dy + ", y: " + y);
	}

	Rectangle getRectangulo() { return Rectangle(x, y, w, h); }

	bool getVisible() { return visible; }

	void setVisible(bool value) { visible = value; }

	int getX() { return x + w; }					// solo se mueve horizontalmente

	int getY() { return y; }

	int getDireccion() { return direccion; }

protected:
	~EnemyTwo() {}

};