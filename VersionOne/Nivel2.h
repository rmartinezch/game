#pragma once
#include "Controller.h";

namespace Game {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Media;

	/// <summary>
	/// Summary for Nivel2
	/// </summary>
	public ref class Nivel2 : public System::Windows::Forms::Form
	{
		// Instancias controlador, graphic, sonido y bitmaps
		Graphics^ graphic;
		BufferedGraphicsContext^ context;
		BufferedGraphics^ buffer;
		SoundPlayer^ sound;
		Bitmap^ bmpGamer;
		Bitmap^ bmpEnemyOne;
		Bitmap^ bmpEnemyTwo;
		Bitmap^ bmpMap;
		Bitmap^ bmpShot;
		Controller* controlador;
		Shot* shot;

	private: System::Windows::Forms::Panel^ pnlLevel2;
	private: System::Windows::Forms::Label^ lblScore2;
	private: System::Windows::Forms::Timer^ timer2;
	private: System::ComponentModel::IContainer^ components;

	public:
		Nivel2(void)
		{
			InitializeComponent();
			this->CenterToScreen();			// Centrar a la pantalla
			graphic = pnlLevel2->CreateGraphics();
			context = BufferedGraphicsManager::Current;
			buffer = context->Allocate(graphic, this->ClientRectangle);

			// Se cargan los bitmaps con las archivos PNG
			bmpGamer = gcnew Bitmap("images/gamer.png");
			bmpMap = gcnew Bitmap("images/map.jpg");
			bmpEnemyOne = gcnew Bitmap("images/one.png");
			bmpEnemyTwo = gcnew Bitmap("images/two.png");
			bmpShot = gcnew Bitmap("images/shot.png");

			// el controlador es creado, se establece el n�mero de enemigos de tipo 1, y se generan los enemigos de tipo 1
			controlador = new Controller(bmpGamer);
			controlador->setNumOneEnemys(10);
			controlador->setNumTwoEnemys(10);
			controlador->fillEnemys(bmpEnemyOne->Width / 3, bmpEnemyOne->Height / 4);
		}

	protected:
		~Nivel2()
		{
			if (components)
			{
				delete components;
			}
		}

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->pnlLevel2 = (gcnew System::Windows::Forms::Panel());
			this->lblScore2 = (gcnew System::Windows::Forms::Label());
			this->timer2 = (gcnew System::Windows::Forms::Timer(this->components));
			this->SuspendLayout();
			// 
			// pnlLevel2
			// 
			this->pnlLevel2->Dock = System::Windows::Forms::DockStyle::Top;
			this->pnlLevel2->Location = System::Drawing::Point(0, 0);
			this->pnlLevel2->Name = L"pnlLevel2";
			this->pnlLevel2->Size = System::Drawing::Size(784, 532);
			this->pnlLevel2->TabIndex = 0;
			// 
			// lblScore2
			// 
			this->lblScore2->Dock = System::Windows::Forms::DockStyle::Bottom;
			this->lblScore2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->lblScore2->Location = System::Drawing::Point(0, 535);
			this->lblScore2->Name = L"lblScore2";
			this->lblScore2->Size = System::Drawing::Size(784, 26);
			this->lblScore2->TabIndex = 1;
			this->lblScore2->Text = L"Vidas: 0 Puntos: 0 Enemigos: 0";
			this->lblScore2->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// timer2
			// 
			this->timer2->Enabled = true;
			this->timer2->Tick += gcnew System::EventHandler(this, &Nivel2::timer2_Tick);
			// 
			// Nivel2
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(784, 561);
			this->Controls->Add(this->lblScore2);
			this->Controls->Add(this->pnlLevel2);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->Name = L"Nivel2";
			this->Text = L"Nivel 2";
			this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &Nivel2::Nivel2_FormClosed);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Nivel2::Nivel2_KeyDown);
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void Nivel2_FormClosed(System::Object^ sender, System::Windows::Forms::FormClosedEventArgs^ e) {
		timer2->Enabled = false;
	}
	private: System::Void timer2_Tick(System::Object^ sender, System::EventArgs^ e) {
		int width = (int)buffer->Graphics->VisibleClipBounds.Width;
		int height = (int)buffer->Graphics->VisibleClipBounds.Height;

		buffer->Graphics->Clear(SystemColors::Control);
		buffer->Graphics->DrawImage(bmpMap, 0, 0, width, height);

		/* el controlador inicia la verificaci�n de golpes, movimiento de los elementos y los dibuja en el panel */
		controlador->hit(buffer->Graphics);
		controlador->movePanel(buffer->Graphics);
		controlador->drawPanel(buffer->Graphics, bmpShot, bmpGamer, bmpEnemyOne, bmpEnemyTwo);

		// actualizamos el score		
		lblScore2->Text = gcnew String(controlador->updateScore().c_str());

		if ((controlador->getNumOneEnemys() + controlador->getNumTwoEnemys()) <= 0) {			// El jugador elimin� a todos los enemigos
			timer2->Enabled = false;
			MessageBox::Show("Superaste el Nivel 2, Empieza nuevamente desde el Nivel 1 ...", "You Win !!!", MessageBoxButtons::OK, MessageBoxIcon::Asterisk);
			this->Close();
		}
		else if (controlador->getGamer()->getVidas() <= 0)	// El jugador no tiene vidas
		{
			timer2->Enabled = false;
			MessageBox::Show("Game over !!! Empieza otra vez ...", "You Lost !!!", MessageBoxButtons::OK, MessageBoxIcon::Error);
			this->Close();
		}
		else
		{
			buffer->Render(graphic);
		}
	}
	private: System::Void Nivel2_KeyDown(System::Object^ sender, System::Windows::Forms::KeyEventArgs^ e) {
		/* Se utilizan las teclas flechas (movimiento del Gamer) y la barra espaciadora (disparo de la bomba) */
		switch (e->KeyCode)
		{
		case Keys::Down:
			controlador->getGamer()->mover(buffer->Graphics, 'D');
			Debug::WriteLine("Flecha abajo");
			break;
		case Keys::Left:
			controlador->getGamer()->mover(buffer->Graphics, 'L');
			Debug::WriteLine("Flecha izquierda");
			break;
		case Keys::Right:
			controlador->getGamer()->mover(buffer->Graphics, 'R');
			Debug::WriteLine("Flecha derecha");
			break;
		case Keys::Up:
			controlador->getGamer()->mover(buffer->Graphics, 'U');
			Debug::WriteLine("Flecha arriba");
			break;
		case Keys::Space:
			/* cada vez que se presiona la tecla espaciadora, se crea un nuevo shot en la posici�n actual del gamer y se le pasa la controlador */
			shot = new Shot(controlador->getGamer()->getX(), controlador->getGamer()->getY(), bmpShot->Width, bmpShot->Height, controlador->getGamer()->getDireccion());
			controlador->addShot(shot);
			Debug::WriteLine("Bomba");
			break;
		default:
			break;
		}
	}
	};
}
