#pragma once
#include "Controller.h"
#include "Nivel2.h"

namespace Game {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Media;
	using namespace System::Diagnostics;	// Debug::WriteLine("mensaje");

	/// <summary>
	/// La Clase Nivel 1 es el primer nivel de difcultad que tiene el juego, esta clase trabaja directamente con el controlador
	/// </summary>
	public ref class Nivel1 : public System::Windows::Forms::Form
	{
		/// se declaran las variables privadas de las clase Nivel1
	private:
		// Instancias controlador, graphic, sonido y bitmaps
		Graphics^ graphic;
		BufferedGraphicsContext^ context;
		BufferedGraphics^ buffer;
		SoundPlayer^ sound;
		Bitmap^ bmpGamer;
		Bitmap^ bmpEnemyOne;
		Bitmap^ bmpMap;
		Bitmap^ bmpShot;
		Controller* controlador;
		Shot* shot;

	private: System::Windows::Forms::Panel^ pnlNivel1;
	private: System::Windows::Forms::Timer^ timer1;

	private: System::Windows::Forms::Label^ lblScore1;

	private: System::ComponentModel::IContainer^ components;

	public:
		Nivel1(void)
		{
			InitializeComponent();
			this->CenterToScreen();			// Centrar el formulario en la pantalla

			graphic = pnlNivel1->CreateGraphics();
			context = BufferedGraphicsManager::Current;
			buffer = context->Allocate(graphic, this->ClientRectangle);

			// Se cargan los bitmaps con las archivos PNG
			bmpGamer = gcnew Bitmap("images/gamer.png");
			bmpMap = gcnew Bitmap("images/map.jpg");
			bmpEnemyOne = gcnew Bitmap("images/one.png");
			bmpShot = gcnew Bitmap("images/shot.png");

			// el controlador es creado, se establece el n�mero de enemigos de tipo 1, y se generan los enemigos de tipo 1
			controlador = new Controller(bmpGamer);
			controlador->setNumOneEnemys(10);
			controlador->fillEnemys(bmpEnemyOne->Width / 3, bmpEnemyOne->Height / 4);

			Debug::WriteLine("Instancia Controller ok !!!");
		}

	protected:
		~Nivel1()
		{
			if (components)
			{
				delete components;
			}
		}

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->pnlNivel1 = (gcnew System::Windows::Forms::Panel());
			this->lblScore1 = (gcnew System::Windows::Forms::Label());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->SuspendLayout();
			// 
			// pnlNivel1
			// 
			this->pnlNivel1->Dock = System::Windows::Forms::DockStyle::Top;
			this->pnlNivel1->Location = System::Drawing::Point(0, 0);
			this->pnlNivel1->Name = L"pnlNivel1";
			this->pnlNivel1->Size = System::Drawing::Size(784, 532);
			this->pnlNivel1->TabIndex = 0;
			// 
			// lblScore1
			// 
			this->lblScore1->BackColor = System::Drawing::SystemColors::Control;
			this->lblScore1->Dock = System::Windows::Forms::DockStyle::Bottom;
			this->lblScore1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->lblScore1->Location = System::Drawing::Point(0, 535);
			this->lblScore1->Name = L"lblScore1";
			this->lblScore1->Size = System::Drawing::Size(784, 26);
			this->lblScore1->TabIndex = 0;
			this->lblScore1->Text = L"Vidas: 0 Puntos: 0 Enemigos: 0";
			this->lblScore1->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Tick += gcnew System::EventHandler(this, &Nivel1::timer_Tick);
			// 
			// Nivel1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(784, 561);
			this->Controls->Add(this->lblScore1);
			this->Controls->Add(this->pnlNivel1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->Name = L"Nivel1";
			this->Text = L"Nivel 1";
			this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &Nivel1::Nivel1_FormClosed);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Nivel1::Nivel1_KeyDown);
			this->ResumeLayout(false);

		}
#pragma endregion
		/* Este m�todo controla el desplazamiento del jugador con las teclas del teclado, en realidad, ordena a Gamer trasladarse */
	private: System::Void Nivel1_KeyDown(System::Object^ sender, System::Windows::Forms::KeyEventArgs^ e) {
		/* Se utilizan las teclas flechas (movimiento del Gamer) y la barra espaciadora (disparo de la bomba) */
		switch (e->KeyCode)
		{
		case Keys::Down:
			controlador->getGamer()->mover(buffer->Graphics, 'D');
			Debug::WriteLine("Flecha abajo");
			break;
		case Keys::Left:
			controlador->getGamer()->mover(buffer->Graphics, 'L');
			Debug::WriteLine("Flecha izquierda");
			break;
		case Keys::Right:
			controlador->getGamer()->mover(buffer->Graphics, 'R');
			Debug::WriteLine("Flecha derecha");
			break;
		case Keys::Up:
			controlador->getGamer()->mover(buffer->Graphics, 'U');
			Debug::WriteLine("Flecha arriba");
			break;
		case Keys::Space:
			/* cada vez que se presiona la tecla espaciadora, se crea un nuevo shot en la posici�n actual del gamer y se le pasa la controlador */
			shot = new Shot(controlador->getGamer()->getX(), controlador->getGamer()->getY(), bmpShot->Width, bmpShot->Height, controlador->getGamer()->getDireccion());
			controlador->addShot(shot);
			Debug::WriteLine("Bomba");
			break;
		default:
			break;
		}
	}
		   /* Este evento del timer permite que el controlador realice tareas repetitivas mientras est� habilitado */
	private: System::Void timer_Tick(System::Object^ sender, System::EventArgs^ e) {
		int width = (int)buffer->Graphics->VisibleClipBounds.Width;
		int height = (int)buffer->Graphics->VisibleClipBounds.Height;

		buffer->Graphics->Clear(SystemColors::Control);
		buffer->Graphics->DrawImage(bmpMap, 0, 0, width, height);

		/* el controlador inicia la verificaci�n de golpes, movimiento de los elementos y los dibuja en el panel */
		controlador->hit(buffer->Graphics);
		controlador->movePanel(buffer->Graphics);
		controlador->drawPanel(buffer->Graphics, bmpShot, bmpGamer, bmpEnemyOne);

		// actualizamos el score		
		lblScore1->Text = gcnew String(controlador->updateScore().c_str());

		if (controlador->getNumOneEnemys() == 0) {			// El jugador elimin� a todos los enemigos
			timer1->Enabled = false;
			if (MessageBox::Show("Superaste el Nivel 1, deseas continuar con el Nivel 2 ?", "You Win !!!", MessageBoxButtons::YesNo, MessageBoxIcon::Question) == System::Windows::Forms::DialogResult::Yes)
			{
				// pasar al Nivel 2
				this->Hide();
				Nivel2^ nivel2 = gcnew Nivel2();
				nivel2->ShowDialog();
				//this->Show();
				this->Close();
			}
			else
			{
				this->Close();
			}
		}
		else if (controlador->getGamer()->getVidas() == 0)	// El jugador no tiene vidas
		{
			timer1->Enabled = false;
			MessageBox::Show("Game over !!!");
			this->Close();
		}
		else
		{
			buffer->Render(graphic);
		}

		//Debug::WriteLine("Temporizador !!!");
	}
		   /* Deshabilita el timer cuando se cierra el formulario */
	private: System::Void Nivel1_FormClosed(System::Object^ sender, System::Windows::Forms::FormClosedEventArgs^ e) {
		Debug::WriteLine("Deshabilitando el timer ...");
		timer1->Enabled = false;
	}
	};
}
