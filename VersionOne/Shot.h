#pragma once
using namespace System::Drawing;
using namespace System::Diagnostics;	// Debug::WriteLine("mensaje");

class Shot {	
	int x, y;			// posici�n y direcci�n del shot
	char direccion;
	int dx, dy;			// magnitud del paso con la que avanza el shot
	int w, h;			// dimensiones (ancho, alto) y visibilidad de la figura que representa al shot
	bool visible;
public:
	Shot(int _x, int _y, int _w, int _h, char _direccion) {
		x = _x;
		y = _y;
		w = _w;
		h = _h;
		dx = 25;		// paso de magnitud 25
		dy = 25;
		direccion = _direccion;
		visible = true;	// todos los shots creados est�n vivos
	}
	
	void draw(Graphics^ graphic, Bitmap^ bitmap) {
		graphic->DrawImage(bitmap, x, y, w, h);
	}

	/* su movimiento est� limitado solo al contexto del panel */
	void move(Graphics^ graphics) {
		if (direccion.Equals('D') && (y+h) < graphics->VisibleClipBounds.Height) { y += dy; }	// avanza hacia arriba
		if (direccion.Equals('U') && 0 < y) { y -= dy; }										// avanza hacia abajo
		if (direccion.Equals('R') && (x+w) < graphics->VisibleClipBounds.Width) { x += dx; }	// avanza hacia la derecha
		if (direccion.Equals('L') && 0 < x) { x -= dx; }										// avanza hacia la izquierda
		//Debug::WriteLine("Shot | dx: " + dx + ", x: " + x + "| dy: " + dy + ", y: " + y);
	}

	int getScopeX() { return x + w; }								// posici�n del shot mas el ancho de su figura en el eje x
	
	int getScopeY() { return y + h; }

	int getX() { return x; }										// posici�n actual del shot en el eje x
	
	int getY() { return y; }

	bool getVisible() { return visible; }							// obtiene la existencia del shot o no vivo (true), destruido (false)

	void setVisible(bool value) { visible = value; }				// establece si el shot est� existe (true) o no (false)

	Rectangle getRectangulo() { return Rectangle(x, y, w, h); }		// retorna un rectangulo que representa la posici�n del shot

protected:
	~Shot() {}
};