#pragma once
#include <vector>
#include <string>
#include "Shot.h"
#include "Gamer.h"
#include "EnemyOne.h"
#include "EnemyTwo.h"

using namespace std;
using namespace System::Diagnostics;	// Debug::WriteLine("mensaje");

/*Clase que controla el desarrollo del juego*/
class Controller {
	Gamer* gamer;								// Clase jugador

	vector<EnemyOne*>	soldiers;				// Enemigos tipo 1
	vector<EnemyTwo*>	marines;				// Enemigos tipo 2

	vector<Shot*>		shots;					// Disparos

	EnemyOne* one;
	EnemyTwo* two;
	int numOneEnemys;							// n�mero de enemigos tipo 1
	int numTwoEnemys;							// n�mero de enemigos tipo 2

public:
	/* Para crear la instancia de Controller, se recibe una imagen, que es utilizada por la instancia de la Clase Gamer */
	Controller(Bitmap^ bmp) {
		gamer = new Gamer(bmp->Width / 3, bmp->Height / 4, 0, 0);
		Debug::WriteLine("Gamer: w= " + bmp->Width + ", h= " + bmp->Height);
		numOneEnemys = 0;						// por defecto se generan 0 enemigos tipo 1
		numTwoEnemys = 0;						// por defecto se generan 0 enemigos tipo 2
	}

	/* Esta funci�n recupera la instancia �nica del Gamer */
	Gamer* getGamer() { return gamer; }

	/* Se crean los enemigos */
	void fillEnemys(int ancho, int alto) {
		/* Enemigos tipo 1 */
		for (int i = 0; i < numOneEnemys; i++)
		{
			one = new EnemyOne(ancho, alto);	// creamos la cantidad de enemigos del primer tipo
			soldiers.push_back(one);			// los inyectamos en el vector de enemigos del primer tipo
		}
		/* Enemigos tipo 2 */
		for (int i = 0; i < numTwoEnemys; i++)
		{
			two = new EnemyTwo(ancho, alto);	// creamos la cantidad de enemigos del primer tipo
			marines.push_back(two);				// los inyectamos en el vector de enemigos del primer tipo
		}
	}

	/* Agregamos municiones con cada disparo */
	void addShot(Shot* shot) {
		shots.push_back(shot);
	}

	/* Movimiento de todos los elementos creados en los vectores */
	void movePanel(Graphics^ graphic) {

		/* shots */
		for (size_t i = 0; i < shots.size(); i++)
		{
			shots.at(i)->move(graphic);
		}

		/* enemigos tipo 1 */
		for (size_t i = 0; i < soldiers.size(); i++)
		{
			soldiers.at(i)->move(graphic);
		}

		/* enemigos tipo 2 */
		for (size_t i = 0; i < marines.size(); i++)
		{
			marines.at(i)->move(graphic);
		}
	}

	/* Se dibujan todos los elementos en el panel 2*/
	void drawPanel(Graphics^ g, Bitmap^ bmpShot, Bitmap^ bmpGamer, Bitmap^ bmpEnemyOne, Bitmap^ bmpEnemyTwo) {
		drawPanel(g, bmpShot, bmpGamer, bmpEnemyOne);
		/* enemigos tipo 2 */
		for (size_t i = 0; i < marines.size(); i++)
		{
			marines.at(i)->draw(g, bmpEnemyTwo);
		}
	}

	/* Se dibujan todos los elementos en el panel 1*/
	void drawPanel(Graphics^ g, Bitmap^ bmpShot, Bitmap^ bmpGamer, Bitmap^ bmpEnemyOne) {

		/* shots */
		for (size_t i = 0; i < shots.size(); i++)
		{
			shots.at(i)->draw(g, bmpShot);
		}
		/* enemigos tipo 1 */
		for (size_t i = 0; i < soldiers.size(); i++)
		{
			soldiers.at(i)->draw(g, bmpEnemyOne);
		}
		gamer->dibujar(g, bmpGamer);
	}

	/* Se verifican las diferentes posiciones entre los elementos en el panel, y se toman acciones de acuerdo con su posicion */
	void hit(Graphics^ graphics) {

		/* Si el shot no se encuentra dentro del contexto del panel, no se visualiza */
		for (size_t i = 0; i < shots.size(); i++)
		{
			if ((graphics->VisibleClipBounds.Width <= shots[i]->getScopeX()) ||
				(graphics->VisibleClipBounds.Height <= shots[i]->getScopeY()) ||
				(shots[i]->getX() <= 0) ||
				(shots[i]->getY() <= 0))
			{
				shots[i]->setVisible(false);
				Debug::WriteLine("invisible !!!");
			}
			//Debug::WriteLine("graphics->VisibleClipBounds.Width: " + graphics->VisibleClipBounds.Width + ", shots[i]->getScopeX()" + shots[i]->getScopeX());
			//Debug::WriteLine("shots[i]->getX(): " + shots[i]->getX());
		}

		/* Verificar si algun shot golpea (hit) a un enemigo tipo 1 */
		for (size_t i = 0; i < soldiers.size(); i++)
		{
			for (size_t j = 0; j < shots.size(); j++)
			{
				if (soldiers[i]->getRectangulo().IntersectsWith(shots[j]->getRectangulo()))
				{
					soldiers[i]->setVisible(false);
					shots[j]->setVisible(false);
					gamer->addPuntaje(10);				// obtiene 10 puntos por cada impacto entre el enemigo y los disparos
					numOneEnemys--;						// un enemigo es eliminado con el shot
				}
			}
		}
		
		/* Verificar si algun shot golpea (hit) a un enemigo tipo 2 */
		for (size_t i = 0; i < marines.size(); i++)
		{
			for (size_t j = 0; j < shots.size(); j++)
			{
				if (marines[i]->getRectangulo().IntersectsWith(shots[j]->getRectangulo()))
				{
					marines[i]->setVisible(false);
					shots[j]->setVisible(false);
					gamer->addPuntaje(20);				// obtiene 20 puntos por cada impacto entre el enemigo y los disparos
					numTwoEnemys--;						// un enemigo es eliminado con el shot
				}
			}
		}

		/* Golpes entre el jugador y los enemigos tipo 1 */
		for (size_t i = 0; i < soldiers.size(); i++)
		{
			if (gamer->getRectangle().IntersectsWith(soldiers[i]->getRectangulo()))
			{
				soldiers[i]->setVisible(false);
				gamer->reduceVidas();					// reduce las vidas del jugador
				numOneEnemys--;							// un enemigo es eliminado con la vida del jugador
			}
		}

		/* Golpes entre el jugador y los enemigos tipo 2 */
		for (size_t i = 0; i < marines.size(); i++)
		{
			if (gamer->getRectangle().IntersectsWith(marines[i]->getRectangulo()))
			{
				marines[i]->setVisible(false);
				gamer->reduceVidas();					// reduce las vidas del jugador
				numTwoEnemys--;							// un enemigo es eliminado con la vida del jugador
			}
		}

		/* Eliminar todos los elementos marcados con false */
		for (size_t i = 0; i < shots.size(); i++)
		{
			if (!shots[i]->getVisible())
			{
				shots.erase(shots.begin() + i);			// borrar el shot en la posici�n donde es marcado como false
			}
		}
		for (size_t i = 0; i < soldiers.size(); i++)
		{
			if (!soldiers[i]->getVisible())
			{
				soldiers.erase(soldiers.begin() + i);	// borrar el enemigo en la posici�n donde es marcado como false
			}
		}
		for (size_t i = 0; i < marines.size(); i++)
		{
			if (!marines[i]->getVisible())
			{
				marines.erase(marines.begin() + i);	// borrar el enemigo en la posici�n donde es marcado como false
			}
		}
	}

	/* devuelve el score de todas las variables como las vidas, el puntaje y el tiempo */
	string updateScore() {
		return	"Vidas: " + std::to_string(gamer->getVidas()) +
				" Puntaje: " + std::to_string(gamer->getPuntaje()) +
				" Enemigos: " + std::to_string(numOneEnemys+numTwoEnemys);
	}

	int getNumOneEnemys() { return numOneEnemys; }
	/* establece cuantos enemigos del tipo uno se tendr� en el inicio del juego */
	void setNumOneEnemys(int value) { numOneEnemys = value; }

	int getNumTwoEnemys() { return numTwoEnemys; }
	/* establece cuantos enemigos del tipo 2 se tendr� en el inicio del juego */
	void setNumTwoEnemys(int value) { numTwoEnemys = value; }

protected:
	~Controller() {}

};