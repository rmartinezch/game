#pragma once
#include <iostream>	// rand()
using namespace System::Drawing;

/* Enemigo que solo se mueve horizontalmente */
class EnemyOne {
	int w, h;		// ancho y alto
	int x, y;		// posicion
	int dx, dy;		// 
	int idx, idy;
	bool visible;
	char direccion;
public:
	EnemyOne(int ancho, int alto) {
		w = ancho;
		h = alto;
		x = rand() % (784-10-10+1)+10;	// posici�n inicial aleatoria
		y = rand() % (561-10-10+1)+10;
		dx = 10;
		idx = 0;
		idy = 2;
		visible = true;
		direccion = 'R';
	}

	/* Dibujar el enemigo */
	void draw(Graphics^ graphic, Bitmap^ bitmap) {
		Rectangle seccion = Rectangle(w * idx, h * idy, w, h);						// secci�n obtenida proveniente del archivo png (bitmap)
		Rectangle zoom = Rectangle(x, y, w, h);										// posici�n y dimensi�n del enemigo
		graphic->DrawImage(bitmap, zoom, seccion, GraphicsUnit::Pixel);
	}

	/* Movimiento del enemigo de forma autom�tica */
	void move(Graphics^ graphic) {
		// Este enemigo solo se mueve horizontalmente
		if (x <= 0 || graphic->VisibleClipBounds.Width <= (x + w)) { dx *= -1; }	// si se ubica m�s all� del contexto del panel, cambio de sentido
		if (0 < dx) { idy = 2; direccion = 'R'; }	// derecha
		if (dx < 0) { idy = 1; direccion = 'L'; }	// izquierda
		idx++;
		if (2 < idx) { idx = 0; }
		x += dx;									// se mueve permanente en direcci�n horizontal
		//Debug::WriteLine("EnemyOne | dx: " + dx + ", x: " + x + "| dy: " + dy + ", y: " + y);
	}

	Rectangle getRectangulo() { return Rectangle(x, y, w, h); }

	bool getVisible() { return visible; }

	void setVisible(bool value) { visible = value; }

	int getX() { return x + w; }					// solo se mueve horizontalmente

	int getY() { return y; }

	int getDireccion() { return direccion; }

protected:
	~EnemyOne() {}

};